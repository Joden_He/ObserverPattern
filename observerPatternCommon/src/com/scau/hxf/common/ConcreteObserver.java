package com.scau.hxf.common;

/**
 * 具体观察者的对象，实现更新的方法，使自身的状态和目标的状态保持一致
 * @author Joden_He
 *
 */
public class ConcreteObserver implements Observer {
	//观察者状态
	private String observerState;
	
	/**
	 * 获取目标类的状态同步到观察者状态中
	 */
	@Override
	public void update(Subject subject) {
		observerState = ((ConcreteSubject)subject).getSubjectState();
	}

	public String getObserverState() {
		return observerState;
	}

	public void setObserverState(String observerState) {
		this.observerState = observerState;
	}

}
