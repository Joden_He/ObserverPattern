package com.scau.hxf.demo;

public class Client {

	public static void main(String[] args) {
		//1 创建目标
		ConcreteWeatherSubject weatherSubject = new ConcreteWeatherSubject();
		
		//2 创建观察者
		ConcreteObserver observer1 = new ConcreteObserver();
		observer1.setObserverName("女朋友");
		observer1.setRemindThing("是我们的第一次约会，地点是街心公园，不见不散哦～～");
		ConcreteObserver observer2 = new ConcreteObserver();
		observer2.setObserverName("妈妈");
		observer2.setRemindThing("是一个购物的好日子");

		//3 注册观察者
		weatherSubject.attach(observer1);
		weatherSubject.attach(observer2);
		
		//4 目标发布天气
		weatherSubject.setWeatherContent("明天天气晴朗，蓝天白云，气温25度");
	}

}
