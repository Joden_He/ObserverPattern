package com.scau.hxf.demo;

/**
 * 具体的目标对象，负责把有关状态存入相关观察者对象中
 * @author Joden_He
 *
 */
public class ConcreteWeatherSubject extends WeatherSubject {
	//获取天气的内容信息
	private String weatherContent;
	

	public String getWeatherContent() {
		return weatherContent;
	}

	public void setWeatherContent(String subjectState) {
		this.weatherContent = subjectState;
		//内容有了通知所有观察者
		this.notifyObservers();
	}
	
}
