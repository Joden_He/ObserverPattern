package com.scau.hxf.demo;

import java.util.ArrayList;
import java.util.List;

/**
 * 目标对象，它知道观察它的观察者，并提供注册（添加）和删除观察者的接口
 * @author Joden_He
 *
 */
public class WeatherSubject {

	//用来保存注册的观察者对象
	private List<Observer> observers = new ArrayList<Observer>();
	
	/**
	 * 添加观察者
	 * @param observer 观察者对象
	 */
	public void attach(Observer observer){
		observers.add(observer);
	}
	
	/**
	 * 删除集合中指定的观察者
	 * @param observer 观察者对象
	 */
	public void detach(Observer observer){
		observers.remove(observer);
	}
	
	/**
	 * 通知所有注册的观察者对象
	 */
	protected void notifyObservers(){
		for (Observer observer : observers) {
			observer.update(this);
		}
	}
	
	
}
