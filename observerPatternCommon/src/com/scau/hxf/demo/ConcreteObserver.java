package com.scau.hxf.demo;

/**
 * 具体观察者的对象，实现更新的方法，使自身的状态和目标的状态保持一致
 * @author Joden_He
 *
 */
public class ConcreteObserver implements Observer {
	
	private String observerName;//观察者的名字
	
	private String weatherContent;//天气情况
	
	private String remindThing;//提醒内容
	
	/**
	 * 获取目标类的状态同步到观察者状态中
	 */
	@Override
	public void update(WeatherSubject subject) {
		weatherContent = ((ConcreteWeatherSubject)subject).getWeatherContent();
		System.out.println(observerName+"收到了"+weatherContent+","+remindThing);
	}

	public String getObserverName() {
		return observerName;
	}

	public void setObserverName(String observerName) {
		this.observerName = observerName;
	}

	public String getWeatherContent() {
		return weatherContent;
	}

	public void setWeatherContent(String weatherContent) {
		this.weatherContent = weatherContent;
	}

	public String getRemindThing() {
		return remindThing;
	}

	public void setRemindThing(String remindThing) {
		this.remindThing = remindThing;
	}

}
